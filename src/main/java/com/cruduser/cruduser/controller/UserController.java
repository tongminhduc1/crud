package com.cruduser.cruduser.controller;

import com.cruduser.cruduser.dto.request.UserCreationRequest;
import com.cruduser.cruduser.entity.User;
import com.cruduser.cruduser.service.UserService;
import jakarta.persistence.Id;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/create")
    User createUser(@RequestBody UserCreationRequest request){
        return userService.createRequest(request);

    }

    @GetMapping()
    Iterable<User> getUser(){
        return userService.findAll();
    }

    @DeleteMapping("/delete")
    String deleteUser(@RequestParam long id){
        return userService.deleteUserById(id);
    }

    @PatchMapping("/update/{id}")
    ResponseEntity<User> updateUser(@PathVariable long id, @RequestBody UserCreationRequest user){
        return userService.updateUser(id, user);
    }
}
