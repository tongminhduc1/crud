package com.cruduser.cruduser.controller;

import com.cruduser.cruduser.dto.request.AccountCreateDTO;
import com.cruduser.cruduser.dto.request.AccountUpdateDTO;
import com.cruduser.cruduser.entity.Account;
import com.cruduser.cruduser.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AccountController {
    @Autowired
    private AccountService accountService;

    @PostMapping("/account")
    Account createAccount(@RequestBody AccountCreateDTO request){
        return accountService.createAccount(request);
    }

    @GetMapping("/account")
    List<Account> getListAccount(){
        return accountService.getListAccount();
    }

    @GetMapping("/account/{id}")
    Account getAccountById(@PathVariable long id){
        return accountService.getAccountById(id);
    }

    @PutMapping("/account/{id}")
    Account updateAccount(@PathVariable long id, @RequestBody AccountUpdateDTO request){
        return accountService.updateAccount(id,request);
    }

    @DeleteMapping("/account/{id}")
    String deleteAccount(@PathVariable long id){
        accountService.deleteAccount(id);
        return "xoa thanh cong";
    }

}
