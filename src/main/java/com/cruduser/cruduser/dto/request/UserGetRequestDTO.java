package com.cruduser.cruduser.dto.request;

import com.cruduser.cruduser.entity.User;

public class UserGetRequestDTO {
    private Iterable<User> users;

    public Iterable<User> getUsers() {
        return users;
    }
}
