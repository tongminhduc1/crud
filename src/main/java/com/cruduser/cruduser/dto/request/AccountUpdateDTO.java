package com.cruduser.cruduser.dto.request;

public class AccountUpdateDTO {
    private String password;
    private int age;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
