package com.cruduser.cruduser.service;

import com.cruduser.cruduser.dto.request.AccountCreateDTO;
import com.cruduser.cruduser.dto.request.AccountUpdateDTO;
import com.cruduser.cruduser.entity.Account;
import com.cruduser.cruduser.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountService {
    @Autowired
    private AccountRepository accountRepository;
    public Account createAccount(AccountCreateDTO request){
        Account account = new Account();
        account.setUsername(request.getUsername());
        account.setPassword(request.getPassword());
        account.setAge(request.getAge());
        return accountRepository.save(account);
    }

    public List<Account> getListAccount(){
        return accountRepository.findAll();
    }

    public Account getAccountById(long id){
         return accountRepository.findById(id).orElseThrow(() -> new RuntimeException("khong ton tai account"));
    }

    public Account updateAccount(long id ,AccountUpdateDTO request){
        Account account = getAccountById(id);
        account.setPassword(request.getPassword());
        account.setAge(request.getAge());
        return accountRepository.save(account);
    }

    public void deleteAccount(long id){
         accountRepository.deleteById(id);
    }

}
