package com.cruduser.cruduser.service;

import com.cruduser.cruduser.dto.request.UserCreationRequest;
import com.cruduser.cruduser.entity.User;
import com.cruduser.cruduser.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public User createRequest(UserCreationRequest request){
        User user = new User();
        user.setUserName(request.getUserName());
        user.setFullName(request.getFullName());
        user.setEmail(request.getEmail());
        user.setPhone(request.getPhone());
        user.setCreatedDate(request.getCreatedDate());
        user.setCreatedBy(request.getCreatedBy());
        user.setUpdatedDate(request.getUpdatedDate());
        user.setUpdatedBy(request.getUpdatedBy());
        user.setStatus(request.getStatus());

       return userRepository.save(user);
    }

    public Iterable<User> findAll() {
        return userRepository.findAll();
    }
    public String deleteUserById(long id){
        Optional<User> isUserExist = userRepository.findById(id);
        System.out.println("isUserExist"+isUserExist);
         if (isUserExist.isPresent()){
             userRepository.deleteById(id);
             return "delete success";
         }
        return "user not found";
    };
    public ResponseEntity<User> updateUser(long id, UserCreationRequest request){
        Optional<User> isUserExist = userRepository.findById(id);
        if (isUserExist.isPresent()){

            User user = isUserExist.get();
//            user.setId(request.());
            user.setUserName(request.getUserName());
            user.setFullName(request.getFullName());
            user.setEmail(request.getEmail());
            user.setPhone(request.getPhone());
            user.setCreatedDate(request.getCreatedDate());
            user.setCreatedBy(request.getCreatedBy());
            user.setUpdatedDate(request.getUpdatedDate());
            user.setUpdatedBy(request.getUpdatedBy());
            user.setStatus(request.getStatus());
            userRepository.save(user);
            return new ResponseEntity<User>(user, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
