package com.cruduser.cruduser.repository;

import com.cruduser.cruduser.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, Long> {
}
